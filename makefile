.PHONY: all clean html

SOURCES := $(wildcard *.org)
HTMLS = $(patsubst %.org, %.html, $(SOURCES))

all: html

clean:
	-rm *.html

html: $(HTMLS)

%.html: %.org
	emacs --batch --visit $^ --eval "(org-html-export-to-html)"
